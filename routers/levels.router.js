const express = require('express')
const router = express.Router()

const { Level } = require('../models')

const { crud, levelsController } = require('../controllers')

const crudController = crud(Level, 'Level')

router.get('/', levelsController.getLevels)
router.get('/:id', crudController.get)
router.patch('/:id', crudController.edit)
router.delete('/:id', crudController.delete)
router.post('/', crudController.new)

module.exports = router

const express = require('express')
const router = express.Router()

const { Folder } = require('../models')

const { crud, foldersController } = require('../controllers')

const crudController = crud(Folder, 'Folder')

router.get('/', foldersController.getRootFolders)
router.get('/:id', crudController.get)
router.patch('/:id', crudController.edit)
router.delete('/:id', crudController.delete)
router.post('/', crudController.new)

router.get('/:id/content', foldersController.getContent)

module.exports = router

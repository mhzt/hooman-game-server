const express = require('express')
const router = express.Router()

const uploader = require('../utils/uploader.util')
const { assetsController } = require('../controllers')

router.get('/:file', assetsController.getFile)
router.post('/', uploader.single('file'), assetsController.new)

module.exports = router

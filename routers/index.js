const express = require('express')
const router = express.Router()

const levelsRouter = require('./levels.router')
const assetsRouter = require('./assets.router')
const foldersRouter = require('./folders.router')

// Health check route
router.get('/', (_, res) => res.json({ time: Date.now() }))

// APIs
router.use('/levels', levelsRouter)
router.use('/assets', assetsRouter)
router.use('/folders', foldersRouter)

module.exports = router

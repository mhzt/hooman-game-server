module.exports = function (...scopes) {
  return {
    log: function (...args) {
      console.log(
        scopes.map(scope => `[${scope}]`).join(' '),
        ...args
      )
    },
    error: function (...args) {
      console.error(
        scopes.map(scope => `[${scope}]`).join(' '),
        ...args)
    }
  }
}

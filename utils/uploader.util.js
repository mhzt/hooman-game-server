const path = require('path')
const multer = require('multer')
const { v4: uuid } = require('uuid')

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '../', 'uploads'))
  },
  filename: function (req, file, cb) {
    cb(null, uuid())
  }
})

const upload = multer({ storage, limit: 300 * 1000 * 1000 })

module.exports = upload

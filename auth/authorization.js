const { sendError } = require('../utils/response.util')

module.exports = function allow (...permittedRoles) {
  return (request, response, next) => {
    const { user } = request

    if (user &&
      (permittedRoles.includes(user.role) || permittedRoles.includes('all'))) {
      return next()
    } else if ((!user || (user && !user.role)) && permittedRoles.includes('unk')) {
      return next()
    } else {
      sendError(response, 403)
    }
  }
}

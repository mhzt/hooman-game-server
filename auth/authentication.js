const jwt = require('jsonwebtoken')
const { User } = require('../models')

const secret = process.env.SECRET || 'wubba lubba dub dub'

module.exports = async function authorize (request, _, next) {
  const apiToken = request.headers['x-api-token'] || request.query.token || ''

  try {
    const { id } = jwt.verify(apiToken, secret)
    request.user = await User.findById(id)
    request.roleFilter = {
      [request.user.role || 'UNK']: request.user._id || 'UNK'
    }
  } catch (error) {
    request.user = false
  }

  if (request.query.hasOwnProperty('token')) delete request.query.token

  next()
}

const { Folder, Level } = require('../models')
const { sendSuccess, sendError } = require('../utils/response.util')

module.exports.getRootFolders = async function (req, res) {
  const folders = await Folder.find({ parent: null })
  return sendSuccess(res, folders, 200)
}

module.exports.getLevels = async function (req, res) {
  const { type, limit, skip } = req.query
  if (!type) return sendError(res, 400, 'Type is required')

  const levels = await Level.getLevels(type, limit, skip)
  const levelIds = levels.map(level => level.id)
  return sendSuccess(res, levelIds, 200)
}

module.exports.getContent = async function (req, res) {
  const { id } = req.params
  const folder = await Folder.findById(id)
  if (!folder) return sendError(res, 404, 'Folder not found')

  const content = await folder.getContent()
  return sendSuccess(res, content, 200)
}

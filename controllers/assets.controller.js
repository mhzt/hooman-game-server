const path = require('path')
const fs = require('fs')
const { sendError, sendSuccess } = require('../utils/response.util')

module.exports.getFile = async function (req, res) {
  const { file } = req.params
  const filePath = path.join(__dirname, '..', 'uploads', file)
  if (fs.existsSync(filePath)) return res.sendFile(filePath)
  return res.status(404).send()
}

module.exports.new = async function (req, res) {
  const { file } = req
  if (!file) return sendError(res, 400, 'No file provided')
  return sendSuccess(res, { file: file.filename })
}

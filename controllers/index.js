const crud = require('./crud.controller')
const assetsController = require('./assets.controller')
const levelsController = require('./levels.controller')
const foldersController = require('./folders.controller')

module.exports = {
  crud,
  assetsController,
  levelsController,
  foldersController
}

const { Level } = require('../models')
const { sendSuccess, sendError } = require('../utils/response.util')

module.exports.getLevels = async function (req, res) {
  const { type, limit, skip } = req.query
  if (!type) return sendError(res, 400, 'Type is required')

  const levels = await Level.getLevels(type, limit, skip)
  const levelIds = levels.map(level => level.id)
  return sendSuccess(res, levelIds, 200)
}

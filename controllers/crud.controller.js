const {
  generateResponse,
  fetchFromDb,
  sendError,
  sendSuccess,
  populateAll
} = require('../utils/response.util')

module.exports = (Ent, entTitle, populates = []) => {
  return {
    get: function (req, res) {
      const { _id, id } = req.params
      return generateResponse(res,
        populateAll(Ent.findById(_id || id), populates),
        true,
        entTitle
      )
    },

    getAll: function (req, res) {
      const { limit = 20, skip, ...query } = req.query
      return generateResponse(res,
        populateAll(Ent.find(query).limit(limit).skip(skip), populates),
        false,
        entTitle
      )
    },

    new: function (req, res) {
      const newEnt = new Ent({ ...req.body })
      generateResponse(res, newEnt.save(), true)
    },
    edit: function (req, res) {
      const { _id, id } = req.params
      generateResponse(res,
        fetchFromDb(Ent.findOne({ _id: _id || id }), res).then((ent) => {
          Object.entries(req.body || {})
            .forEach(([key, value]) => { ent[key] = value })
          return ent.save()
        }))
    },
    delete: function (req, res) {
      const { _id, id } = req.params
      Ent.findOneAndDelete({ _id: _id || id })
        .then(() => sendSuccess(res, { message: 'Deleted' }))
        .catch(() => sendError(res, 500, 'Error'))
    },
    softDelete: function (req, res) {
      const { _id, id } = req.params
      Ent.findOneAndUpdate({ _id: _id || id }, { deleted: true })
        .then(() => sendSuccess(res, { message: 'Deleted' }))
        .catch(() => sendError(res, 500, 'Error'))
    }
  }
}

require('dotenv').config()

const logger = require('./utils/logger.util')('MAIN')

const express = require('express')
const morgan = require('morgan')

const apiRouter = require('./routers')

const app = express()
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.head('*', (_, res) => res.status(200).send())
app.use('/api', apiRouter)

app.listen(process.env.PORT, () => {
  logger.log(`Server is running on port ${process.env.PORT}`)
})

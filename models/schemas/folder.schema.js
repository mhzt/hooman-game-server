const { Schema, model } = require('mongoose')
const { model: Level } = require('./level.schema')

const FolderSchema = new Schema({
  parent: { type: Schema.Types.ObjectId, ref: 'Folder' },
  title: { type: String, required: true },
  meta: { type: Schema.Types.Mixed, default: {} }
})

FolderSchema.set('toJSON', {
  transform: (_doc, ret) => {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
  }
})

FolderSchema.methods.getLevels = function () {
  return Level.find({ parent: this._id }).select('_id')
}

FolderSchema.methods.getFolders = function () {
  return Folder.find({ parent: this._id })
}

FolderSchema.methods.getContent = async function () {
  const levels = await this.getLevels()
  const folders = await this.getFolders()
  return { levels, folders }
}

const Folder = model('Folder', FolderSchema)

module.exports = { schema: FolderSchema, model: Folder }

const fs = require('fs')
const path = require('path')

const { Schema, model } = require('mongoose')

const AssetSchema = new Schema({
  type: { type: String, required: true, enum: ['image', 'voice', 'text'] },
  role: { type: String, enum: ['question', 'answer'], default: 'answer' },
  content: { type: String },
  isCorrect: { type: Boolean, default: false },
  meta: { type: Schema.Types.Mixed, default: {} }
}, { _id: false })

AssetSchema.pre('save', function (next) {
  if (['image', 'voice'].includes(this.type)) {
    const filePath = path.join(__dirname, '..', '..', 'uploads', this.content)
    if (!fs.existsSync(filePath)) return next(new Error('File does not exist'))
  }
  return next()
})

AssetSchema.set('toJSON', {
  transform: (_doc, ret) => {
    if (['image', 'voice'].includes(ret.type)) { ret.url = `/assets/${ret.content}` }
  }
})

const _levelTypes = [
  'DragAndDrop',
  'Matching',
  'Coloring',
  'Memory_Flip',
  'Ticking',
  'Voice_Over',
  'Connect_DotLine'
]

const _levelDifficulties = [
  'Easy',
  'Intermediate',
  'Hard'
]

const _typeEnums = _levelTypes.map(levelType =>
  _levelDifficulties.map(levelDifficulty =>
    [levelType, levelDifficulty].join('_'))
).reduce((p, c) => [...p, ...c], [])

const LevelSchema = new Schema({
  type: { type: String, required: true, enum: _typeEnums },
  assets: { type: [AssetSchema], default: [] },
  deleted: { type: Boolean, default: false },
  parent: { type: Schema.Types.ObjectId, ref: 'Folder' }
}, { timestamps: true })

LevelSchema.set('toJSON', {
  transform: (_doc, ret) => {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
  }
})

LevelSchema.statics.getLevels = function (type, skip, limit) {
  return this.find({ type }).skip(skip).limit(limit).select('_id')
}

const Level = model('Level', LevelSchema)

module.exports = { schema: LevelSchema, model: Level, typeEnums: _typeEnums }

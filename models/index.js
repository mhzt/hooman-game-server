const mongoose = require('mongoose')
const logger = require('../utils/logger.util')('MDB')

mongoose.Promise = global.Promise

let mongoUrl = process.env.MONGO || 'mongodb://localhost:27017/myapp'
if (!mongoUrl.startsWith('mongodb://')) {
  mongoUrl = 'mongodb://' + mongoUrl
}

mongoose.connect(mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

mongoose.connection.on('connected', () => {
  logger.log('Mongoose connected to ' + mongoUrl)
})

mongoose.connection.on('error', (err) => {
  logger.error('Mongoose connection error: ' + mongoUrl + '\n' + err)
})

mongoose.connection.on('disconnected', () => {
  logger.log('Mongoose disconnected')
})

const { model: Level } = require('./schemas/level.schema')
const { model: Folder } = require('./schemas/folder.schema')

module.exports.Level = Level
module.exports.Folder = Folder
